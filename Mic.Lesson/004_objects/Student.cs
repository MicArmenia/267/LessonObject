﻿namespace _004_objects
{
    class Student
    {
        public Student()
        { }

        public Student(string name = null, string surname = null, int age = 0, string email = null)
        {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.age = age;
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}