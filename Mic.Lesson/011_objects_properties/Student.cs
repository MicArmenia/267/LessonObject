﻿namespace _011_objects_properties
{
    class Student
    {
        private string _name;
        public string Name
        {
            get => _name;
            set
            {
                if(!string.IsNullOrEmpty(value))
                {
                    if (char.IsLetter(value[0]))
                        _name = value;
                }
            }
        }
        public string Surname { get; set; }

        //private string m_name;

        //public string get_Name()
        //{
        //    return m_name;
        //}

        //public void set_Name(string value)
        //{
        //    m_name = value;
        //}
    }
}