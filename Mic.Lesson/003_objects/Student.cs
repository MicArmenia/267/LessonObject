﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_objects
{
    class Student
    {
        public Student()
        { }

        public Student(string name)
        {
            if (!string.IsNullOrEmpty(name) && char.IsLetter(name[0]))
                this.name = name;
        }

        public Student(string name, string surname)
            : this(name)
        {
            //if (!string.IsNullOrEmpty(name) && char.IsLetter(name[0]))
            //    this.name = name;
            this.surname = surname;
        }

        public Student(string name, string surname, int age)
            : this(name, surname)
        {
            //if (!string.IsNullOrEmpty(name) && char.IsLetter(name[0]))
            //    this.name = name;
            //this.surname = surname;
            this.age = age;
        }

        public Student(string name, string surname, string email)
            : this(name, surname)
        {
            this.email = email;
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}