﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _009_objects_properties
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student()
            {
                name = "A1",
                surname = "A1yan",
                Age = 5
            };

            //Console.WriteLine($"{st.name} {st.surname}");
            Console.WriteLine(st.Fullname);
        }
    }
}
