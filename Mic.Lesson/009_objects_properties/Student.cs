﻿namespace _009_objects_properties
{
    class Student
    {
        public string name;
        public string surname;

        public string Fullname => $"{name} {surname}";
        //{
        //    get { return $"{name} {surname}"; }
        //}

        private byte age;
        public byte Age
        {
            //get { return age; }
            get => age;
            set
            {
                if (value < 15 || value > 65)
                    age = 0;
                else
                    age = value;
            }
        }

        //public byte get_Age()
        //{
        //    return age;
        //}

        //public void set_Age(byte value)
        //{
        //    if (value < 15 || value > 65)
        //        age = 0;
        //    else
        //        age = value;
        //}
    }
}