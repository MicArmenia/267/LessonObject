﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_objects
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student();
            Type t = st.GetType();
            string name = st.ToString();
            int code = st.GetHashCode();

            Student st1 = new Student();
            if(st.Equals(st1))
            {

            }

            Student st3 = st;
            if(st3.Equals(st))
            {

            }

            Student st4 = null;
            if(st4 != null)
            {
                int code4 = st4.GetHashCode();
            }

            Console.ReadLine();
        }
    }
}