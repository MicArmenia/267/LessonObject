﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_objects
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student { email = "a1@gmail.com" };
            var st2 = new Student
            {
                name = "A1",
                surname = "A1yan",
                email = "a1@gmail.com",
                age = 17
            };
        }
    }
}
