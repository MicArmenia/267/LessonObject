﻿namespace _007_objects
{
    class Student
    {
        public string name;
        public string surname;
        private byte age;

        public byte GetAge()
        {
            return age;
        }

        public void SetAge(byte value)
        {
            if (value < 15 || value > 65)
                age = 0;
            else
                age = value;
        }
    }
}