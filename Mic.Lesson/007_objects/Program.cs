﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_objects
{
    class Program
    {
        static void Main(string[] args)
        {
            var st = new Student()
            {
                name = "A1",
                surname = "A1yan"
                //age = 5
            };

            st.SetAge(5);

            byte age = st.GetAge();
        }
    }
}
